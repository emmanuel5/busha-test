class DummyData {

  var CryptoDummyData = {

    "crypto_transactions" :  [

      {
        "day": "MAY 15, 2020",
        "transactions" : [
          {
            "image": "btc",
            "crypto_amount" : -2.53,
            "transaction_time" : "11:56 am",
            "transaction_message" : "Sent Bitcoin",
            "transaction_id" : "sdvgfwadlkmwd"
          },
          {
            "image": "eth",
            "crypto_amount" : 0.53,
            "transaction_time" : "11:56 am",
            "transaction_message" : "Bought Bitcoin",
            "transaction_id" : "sdvgfwadlkmwd"
          }
        ]
      },
      {
        "day": "MAY 14, 2020",
        "transactions" : [
          {
            "image": "btc",
            "crypto_amount" : -2.11,
            "transaction_time" : "11:56 am",
            "transaction_message" : "Sent Bitcoin",
            "transaction_id" : "sdvgfwadlkmwd"
          },
          {
            "image": "ltc",
            "crypto_amount" : 0.53,
            "transaction_time" : "11:56 am",
            "transaction_message" : "Sent Bitcoin",
            "transaction_id" : "sdvgfwadlkmwd"
          },
          {
            "image": "btc",
            "crypto_amount" : 0.53,
            "transaction_time" : "11:56 am",
            "transaction_message" : "Sent Bitcoin",
            "transaction_id" : "sdvgfwadlkmwd"
          },
          {
            "image": "btc",
            "crypto_amount" : -2.53,
            "transaction_time" : "11:56 am",
            "transaction_message" : "Sent Bitcoin",
            "transaction_id" : "sdvgfwadlkmwd"
          },
          {
            "image": "btc",
            "crypto_amount" : 0.53,
            "transaction_time" : "11:56 am",
            "transaction_message" : "Bought Bitcoin",
            "transaction_id" : "sdvgfwadlkmwd"
          },
          {
            "image": "eth",
            "crypto_amount" : 2.00,
            "transaction_time" : "11:56 am",
            "transaction_message" : "Bought Bitcoin",
            "transaction_id" : "sdvgfwadlkmwd"
          }
        ]
      },
      {
        "day": "MAY 13, 2020",
        "transactions" : [
          {
            "image": "xlm",
            "crypto_amount" : 0.53,
            "transaction_time" : "11:56 am",
            "transaction_message" : "Sent Bitcoin",
            "transaction_id" : "sdvgfwadlkmwd"
          },
          {
            "image": "eth",
            "crypto_amount" : 0.53,
            "transaction_time" : "11:56 am",
            "transaction_message" : "Sent Bitcoin",
            "transaction_id" : "sdvgfwadlkmwd"
          },
          {
            "image": "eth",
            "crypto_amount" : 0.53,
            "transaction_time" : "11:56 am",
            "transaction_message" : "Sent Bitcoin",
            "transaction_id" : "sdvgfwadlkmwd"
          }
        ]
      }
    ]
  };


}